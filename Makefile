publish:
	rm -rf _site/
	jekyll build
	s3cmd -P -M --exclude="assets/css/*" sync _site/ s3://www.e6h.org/
	# mime magic not working with CSS
	s3cmd -P -m text/css sync _site/assets/css/ s3://www.e6h.org/assets/css/
